#!/usr/bin/env python
"""
Merge ntuples with specific category
"""
from __future__ import print_function
import h5py
import numpy as np
import sys
from parser import get_args_merge as parser
import msg
from merge import get_filelist


def append_events(out_file, app_arr, key='jets'):
    # print('key:', key)
    # print("!!!!!!!!!!")
    with h5py.File(out_file, 'a') as hf:
        hf[key].resize((hf[key].shape[0] + app_arr.shape[0]), axis=0)
        hf[key][-app_arr.shape[0]:] = app_arr
        hf.close()


def extract_events(input_file, category='HadronConeExclExtendedTruthLabelID',
                   cuts=55, key="jets", key_trk=None):
    print("extract double b jets from %s" % input_file)
    file = h5py.File(input_file, 'r')
    # njets = len(file[key])
    # steps = 1000000
    # iterations = njets//steps
    # jets = np.empty_like(file[key][:1])
    # if key_trk is not None:
    #     tracks = np.empty_like(file[key_trk][:1])
    # indices_to_keep = np.array([])
    # print(iterations)
    # for x in range(iterations+1):
    #     jeti = file[key][x:x + steps]
    #     indices_to_keep_i = np.where(jeti[category] == cuts)
    #     indices_to_keep_i = indices_to_keep_i[0] + x * steps
    #     indices_to_keep = np.concatenate((indices_to_keep, indices_to_keep_i))
        # indices_to_remove = np.where(jeti[category] != cuts)
        # jeti = np.delete(jeti, indices_to_remove)[:]
        # jets = np.concatenate((jets, jeti))
        # print(indices_to_remove[0] + x * steps)
        # if key_trk is not None:
        #     tracki = file[key_trk][x:x + steps]
        #     print("tracki:", tracki.shape)
        #     tracki = np.delete(tracki, indices_to_remove)[:]
        #     # if x == 0:
        #     #     tracks = tracki
        #     # else:
        #     tracks = np.append(tracks, tracki, axis=0)
        #     print("tracks:", tracks.shape)
        # if x > 0:
        #     break
    jeti = file[key][:]
    indices_to_keep = np.where(jeti[category] == cuts)[0]
    jets = jeti[indices_to_keep]
    tracks = file[key_trk][indices_to_keep]
    file.close()
    if key_trk is not None:
        return jets[:], tracks[:]
    
    return jets[:]


if __name__ == '__main__':

    msg.box("HDF5 MANIPULATOR: MERGE NTUPLES")

    args = parser(category=True)

    filelist = get_filelist(list(args.input_files))

    if not filelist:
        msg.error("No files matching --input were found.")
        sys.exit(1)

    print("The following input files were found:\n")

    for f in filelist:
        print("\t - %s" % f)
    category = 'bbjets'
    # category = 'bjets'
    for i, f in enumerate(filelist):
        if i == 0:
            with h5py.File(args.output, 'w') as output:
                jets, tracks = extract_events(f, key_trk='tracks')
                print("Found %i bb jets" % len(jets))
                output.create_dataset('jets', maxshape=(None,), data=jets,
                                    compression='gzip')
                output.create_dataset('tracks', data=tracks, maxshape=(None,None),
                                    compression='gzip')
                del jets
                del tracks
        else:
            jets, tracks = extract_events(f, key_trk='tracks')
            print("Found %i bb jets" % len(jets))
            append_events(args.output, jets, key='jets')
            append_events(args.output, tracks, key='tracks')
            del jets
            del tracks

    msg.info("Done")
